package com.walu.fizzBuzz.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.util.MimeTypeUtils;


import com.walu.fizzBuzz.models.FizzBuzzNumbers;



/**
 * Tests for FizzBuzzGame rest controller
 * 
 * @author Walu
 *
 */
@RunWith(SpringRunner.class)
@WebMvcTest(controllers = FizzBuzzGame.class)
public class FizzBuzzGameTests {

	@InjectMocks
	private FizzBuzzGame fizzBuzzGameController;
	
	@Autowired
	FizzBuzzNumbers fizzBuzzNumbers;
    
	@Autowired
	private MockMvc mockMvc;

	
	
	/**
	 * Tests "play" get rest API
	 * 
	 * @throws Exception throws API failed exceptions
	 */
	@Test
	public void testPlay() throws Exception {
		
		final ResultActions result = this.mockMvc.perform(get("/play/{nums}",15).accept(MimeTypeUtils.APPLICATION_JSON_VALUE));
		
		result.andExpect(status().isOk()); // check that the right status is send
		result.andExpect(jsonPath("$.length()").value(1)); // check that the size of array is right
		result.andExpect(jsonPath("$[*].fizzBuzzValue", "Lizz Buzz").exists()); // check that right result is displayed
	
	}

}
