package com.walu.fizzBuzz.models;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.walu.fizzBuzz.models.FizzBuzzNumber;


/**
 * Tests the  FizzBuzzNumber
 * 
 * @author Walu
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class FizzBuzzNumberTests {
	
	
	/**
	 * Test for positive numbers that are not multiple of three nor five
	 */
	@Test
	public void testConversationToFizzBuzz_PositiveNumbersNotDivisibleBy3nor5() {
		
		FizzBuzzNumber fizzBuzzNumber = new FizzBuzzNumber(1);
		assertEquals("1", fizzBuzzNumber.getFizzBuzzValue());
		fizzBuzzNumber = new FizzBuzzNumber(4);
		assertEquals("4", fizzBuzzNumber.getFizzBuzzValue());
		fizzBuzzNumber = new FizzBuzzNumber(10001);
		assertEquals("10001", fizzBuzzNumber.getFizzBuzzValue());	
	}
	
	/**
	 * Test for positive numbers that are multiple of three but not multiple of five
	 */
	@Test
	public void testConversationToFizzBuzz_PositiveNumbersByDivisibleBy3NotBy5() {
		
		FizzBuzzNumber fizzBuzzNumber = new FizzBuzzNumber(3);
		assertEquals("Fizz", fizzBuzzNumber.getFizzBuzzValue());
		fizzBuzzNumber = new FizzBuzzNumber(33);
		assertEquals("Fizz", fizzBuzzNumber.getFizzBuzzValue());
		fizzBuzzNumber = new FizzBuzzNumber(10101);
		assertEquals("Fizz", fizzBuzzNumber.getFizzBuzzValue());	
	}
	
	/**
	 * Test for positive numbers that are multiple of five but not multiple of three
	 */
	@Test
	public void testConversationToFizzBuzz_PositiveNumbersByDivisibleBy5Not3() {
		
		FizzBuzzNumber fizzBuzzNumber = new FizzBuzzNumber(5);
		assertEquals("Buzz", fizzBuzzNumber.getFizzBuzzValue());
		fizzBuzzNumber = new FizzBuzzNumber(500);
		assertEquals("Buzz", fizzBuzzNumber.getFizzBuzzValue());
		fizzBuzzNumber = new FizzBuzzNumber(10015);
		assertEquals("Buzz", fizzBuzzNumber.getFizzBuzzValue());	
	}
	
	/**
	 * Test for positive numbers that are multiple of both three and five 
	 */
	@Test
	public void testConversationToFizzBuzz_PositiveNumbersByDivisibleBy3and5() {
		
		FizzBuzzNumber fizzBuzzNumber = new FizzBuzzNumber(15);
		assertEquals("Fizz Buzz", fizzBuzzNumber.getFizzBuzzValue());
		fizzBuzzNumber = new FizzBuzzNumber(600);
		assertEquals("Fizz Buzz", fizzBuzzNumber.getFizzBuzzValue());
		fizzBuzzNumber = new FizzBuzzNumber(10305);
		assertEquals("Fizz Buzz", fizzBuzzNumber.getFizzBuzzValue());	
	}

	/**
	 * Test for negative numbers that are not multiple of three nor five
	 */
	@Test
    public void testConversationToFizzBuzz_NegativeNumbersNotDivisibleBy3nor5() {
		
		FizzBuzzNumber fizzBuzzNumber = new FizzBuzzNumber(-1);
		assertEquals("-1", fizzBuzzNumber.getFizzBuzzValue());
		fizzBuzzNumber = new FizzBuzzNumber(-4);
		assertEquals("-4", fizzBuzzNumber.getFizzBuzzValue());
		fizzBuzzNumber = new FizzBuzzNumber(-10001);
		assertEquals("-10001", fizzBuzzNumber.getFizzBuzzValue());	
	}
	
    /**
	 * Test for negative numbers that are multiple of three but not multiple of five
	 */
	@Test
	public void testConversationToFizzBuzz_NegativeNumbersByDivisibleBy3NotBy5() {
		
		FizzBuzzNumber fizzBuzzNumber = new FizzBuzzNumber(-3);
		assertEquals("Fizz", fizzBuzzNumber.getFizzBuzzValue());
		fizzBuzzNumber = new FizzBuzzNumber(-33);
		assertEquals("Fizz", fizzBuzzNumber.getFizzBuzzValue());
		fizzBuzzNumber = new FizzBuzzNumber(-10101);
		assertEquals("Fizz", fizzBuzzNumber.getFizzBuzzValue());	
	}
	
	/**
	 * Test for negative numbers that are multiple of five but not multiple of three
	 */
	@Test
	public void testConversationToFizzBuzz_NegativeNumbersByDivisibleBy5Not3() {
		
		FizzBuzzNumber fizzBuzzNumber = new FizzBuzzNumber(-5);
		assertEquals("Buzz", fizzBuzzNumber.getFizzBuzzValue());
		fizzBuzzNumber = new FizzBuzzNumber(-500);
		assertEquals("Buzz", fizzBuzzNumber.getFizzBuzzValue());
		fizzBuzzNumber = new FizzBuzzNumber(-10015);
		assertEquals("Buzz", fizzBuzzNumber.getFizzBuzzValue());	
	}
	
	/**
	 * Test for negative numbers that are multiple of both three and five 
	 */
	@Test
	public void testConversationToFizzBuzz_NegativeNumbersByDivisibleBy3and5() {
		
		FizzBuzzNumber fizzBuzzNumber = new FizzBuzzNumber(-15);
		assertEquals("Fizz Buzz", fizzBuzzNumber.getFizzBuzzValue());
		fizzBuzzNumber = new FizzBuzzNumber(-600);
		assertEquals("Fizz Buzz", fizzBuzzNumber.getFizzBuzzValue());
		fizzBuzzNumber = new FizzBuzzNumber(-10305);
		assertEquals("Fizz Buzz", fizzBuzzNumber.getFizzBuzzValue());	
	}
	
	/**
	 * Test if result of dividing three and five into zero
	 */
	@Test
	public void testConversationToFizzBuzz_Zero() {
		FizzBuzzNumber fizzBuzzNumber = new FizzBuzzNumber(0);
		assertEquals("Fizz Buzz", fizzBuzzNumber.getFizzBuzzValue());	
	}

}
