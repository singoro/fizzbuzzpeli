package com.walu.fizzBuzz.models;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.walu.fizzBuzz.controllers.FizzBuzzGame;

/**
 * Application level sanity check tests
 * 
 * @author Walu
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class FizzBuzzApplicationSmokeTests {
	
	@Autowired
	FizzBuzzGame controller;
     
	/**
	 *  Sanity check test that will fail if the application context cannot start
	 */
	@Test
	public void contextLoads() {
		 assertThat(controller).isNotNull();
	}

}
