package com.walu.fizzBuzz.models;

import static org.junit.Assert.*;

import java.util.ArrayList;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.walu.fizzBuzz.models.FizzBuzzNumber;
import com.walu.fizzBuzz.models.FizzBuzzNumbers;

/**
 * Tests the  FizzBuzzNumbers
 * 
 * @author Walu
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class FizzBuzzNumbersTests {
	@Autowired
	FizzBuzzNumbers fizzBuzzNumbers;
	
	/**
	 * Tests list of FizzBuzzNumber class by comparing the values returned
	 */
	@Test
	public void testTakeNumbersAndReturnFizzBuzzFunction()
	{
		ArrayList<FizzBuzzNumber> eFizzBuzzNumbers = new ArrayList<FizzBuzzNumber>();
		eFizzBuzzNumbers.add(new FizzBuzzNumber(0)); 
		eFizzBuzzNumbers.add(new FizzBuzzNumber(1)); 
		eFizzBuzzNumbers.add(new FizzBuzzNumber(3)); 
		eFizzBuzzNumbers.add(new FizzBuzzNumber(4)); 
		eFizzBuzzNumbers.add(new FizzBuzzNumber(15));
		
		
		assertEquals(eFizzBuzzNumbers.get(0).getFizzBuzzValue(), fizzBuzzNumbers.takeNumbersAndReturnFizzBuzz(new Integer[] {0,1,3,4,15}).get(0).getFizzBuzzValue());
		assertEquals(eFizzBuzzNumbers.get(1).getFizzBuzzValue(), fizzBuzzNumbers.takeNumbersAndReturnFizzBuzz(new Integer[] {0,1,3,4,15}).get(1).getFizzBuzzValue());
		assertEquals(eFizzBuzzNumbers.get(2).getFizzBuzzValue(), fizzBuzzNumbers.takeNumbersAndReturnFizzBuzz(new Integer[] {0,1,3,4,15}).get(2).getFizzBuzzValue());
		assertEquals(eFizzBuzzNumbers.get(3).getFizzBuzzValue(), fizzBuzzNumbers.takeNumbersAndReturnFizzBuzz(new Integer[] {0,1,3,4,15}).get(3).getFizzBuzzValue());
		assertEquals(eFizzBuzzNumbers.get(4).getFizzBuzzValue(), fizzBuzzNumbers.takeNumbersAndReturnFizzBuzz(new Integer[] {0,1,3,4,15}).get(4).getFizzBuzzValue());
		
		
	}

}
