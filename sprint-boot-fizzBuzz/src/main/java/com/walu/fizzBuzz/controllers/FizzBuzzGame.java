package com.walu.fizzBuzz.controllers;


import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.walu.fizzBuzz.models.FizzBuzzNumber;
import com.walu.fizzBuzz.models.FizzBuzzNumbers;

/**
 * FizzBuzz game controller
 * 
 * @author Walu
 *
 */
@Import({ com.walu.fizzBuzz.models.FizzBuzzNumbers.class })
@RestController

public class FizzBuzzGame {
	
	
	private FizzBuzzNumbers fizzBuzzNumbers;

	@Autowired
	FizzBuzzGame(FizzBuzzNumbers fizzBuzzNumbers) {
		this.fizzBuzzNumbers = fizzBuzzNumbers;
	}

	/**
	 * Provides access to play the game
	 * 
	 * @param nums is an array of integers that are to be converted to FizzBuzz format
	 * @return a list of numbers in FizzBuzz format
	 */
	@RequestMapping(method=RequestMethod.GET, value="/play/{nums}")
    public ArrayList<FizzBuzzNumber> play(@PathVariable Integer[] nums) {
		
		return fizzBuzzNumbers.takeNumbersAndReturnFizzBuzz(nums);
    }
	
}
