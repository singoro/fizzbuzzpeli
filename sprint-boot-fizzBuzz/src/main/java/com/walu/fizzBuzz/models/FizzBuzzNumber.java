package com.walu.fizzBuzz.models;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * Contains fields and methods affecting a FizzBuzz number 
 * 
 * @author Walu
 *
 */
public class FizzBuzzNumber {
	
	/**
	 * Intrinsic value of the number
	 */
	@NotNull
	@NotEmpty
	private int actualNumber;
	
	/**
	 * @return the actualNumber
	 */
	public int getActualNumber() {
		return actualNumber;
	}

	
	/**
	 *  Fizzbuzz string format for a number, it can be the number itself, Fizz, Buzz or Fizz Buzz
	 */
	private String fizzBuzzValue;
	
	
	
	/**
	 * @return the fizzBuzzValue
	 */
	public String getFizzBuzzValue() {
		return fizzBuzzValue;
	}

	/**
	* Constructor that takes intrinsic value of the number as a parameter 
	 * 
	 * @param actualNumber is intrinsic value of the number to be converted into FizzBuzz format
	 */
     FizzBuzzNumber(int actualNumber)
	{
		this.actualNumber= actualNumber;
		this.convertActualNumberToFzzBuzzValue();
		
	}
	
	/**
	 * Converts number of integer type to FizzBuzz format
	 */
	private void convertActualNumberToFzzBuzzValue() {
		
		if(this.isNumberDivisibleByThree(getActualNumber())&& this.isNumberDivisibleByFive(getActualNumber()))
    	{
			fizzBuzzValue = "Fizz Buzz";
    	}
    	else if(this.isNumberDivisibleByFive(getActualNumber()))
    	{ 
    		fizzBuzzValue = "Buzz";
    	}
    	else if(this.isNumberDivisibleByThree(getActualNumber()))
    	{
    		fizzBuzzValue = "Fizz";
    	}
    	else
    	{
    		fizzBuzzValue = Integer.toString(getActualNumber());
    	}
	}

	
	/**
	 * Checks if a number is divisible by three
	 * @param number to be checked if it is divisible by three
	 * @return returns true if the number is divisible by three otherwise returns false
	 */
	private boolean isNumberDivisibleByThree(int number)
	 {
			if(number%3 ==0 )
				return true;
			else 
				return false;
	 }
	
	/**
	 * Checks if a number is divisible by five
	 * @param number to be checked if it is divisible by five
	 * @return returns true if the number is divisible by five otherwise returns false
	 */
	private boolean isNumberDivisibleByFive(int number)
	 {
		if(number%5 ==0 )
			return true;
		else 
			return false;
	 }

	
}
