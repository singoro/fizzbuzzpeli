package com.walu.fizzBuzz.models;

import java.util.ArrayList;

import org.springframework.stereotype.Service;


/**
 * Holds a list of FizzBuzzNumber objects
 * 
 * @author Walu
 *
 */

@Service
public class FizzBuzzNumbers {
	
	/**
	 * Get a list of FizzBuzzNumber objects by calling the FizzBuzzNumber class
	 * 
	 * @param integerArray an array of integer numbers
	 * @return a list of FizzBuzzNumber objects
	 */
	public ArrayList <FizzBuzzNumber> takeNumbersAndReturnFizzBuzz(Integer[]  integerArray)
	{
		ArrayList<FizzBuzzNumber> fizzBuzzNumbers = new ArrayList<>();
		
	     for(Integer param : integerArray) {
	    	 
	    	 fizzBuzzNumbers.add(new FizzBuzzNumber(param));    
	     }
	     return fizzBuzzNumbers;
	}
}
