ABOUT THE PROJECT
This project contains 
1. FizzBuzz game. It is a springBoot - the backend of the application

REQUIREMENTS
The following are needed to further develop the project:
	*A favorite text editor or IDE
	*JDK 1.8 or later
	*Gradle 4+ or Maven 3.2+

You can also import the code straight into your IDE:

	-Spring Tool Suite (STS)
	-IntelliJ IDEA

UNIT TESTS
Always run unit tests before making a new installation or checkin code changes

AUTHOR
The document has been drafted by Walubengo Muliro Singoro (08/11/2018)