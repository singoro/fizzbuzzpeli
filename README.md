ABOUT

This project contains two projects for FizzBuzz game:
1. SpringBoot - backend of the application
2. Angular 7 - front end of the application

NOTE
For further instructions on how to take the projects in production or development see README file in each of the project's root folder

AUTHOR
This document was drafted by Walubengo Muliro Singoro(08/11/2018)

