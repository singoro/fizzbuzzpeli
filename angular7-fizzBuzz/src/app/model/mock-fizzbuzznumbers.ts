import { FizzbuzzNumber } from './fizzbuzz-number';

export const FIZZBUZZNUMBERS: FizzbuzzNumber[]= [
 {actualNumber: 1, fizzBuzzValue: '1'},
 {actualNumber: 3, fizzBuzzValue: 'Fizz'},
 {actualNumber: 4, fizzBuzzValue: '4'},
 {actualNumber: 5, fizzBuzzValue: 'Buzz'},
 {actualNumber: 6, fizzBuzzValue: 'Fizz'},
 {actualNumber: 7, fizzBuzzValue: '7'},
 {actualNumber: 8, fizzBuzzValue: '8'},
 {actualNumber: 15,fizzBuzzValue: 'Fizz Buzz'}
];