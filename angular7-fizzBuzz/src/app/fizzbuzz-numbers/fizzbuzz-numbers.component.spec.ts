import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FizzbuzzNumbersComponent } from './fizzbuzz-numbers.component';

describe('FizzbuzzNumbersComponent', () => {
  let component: FizzbuzzNumbersComponent;
  let fixture: ComponentFixture<FizzbuzzNumbersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FizzbuzzNumbersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FizzbuzzNumbersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
