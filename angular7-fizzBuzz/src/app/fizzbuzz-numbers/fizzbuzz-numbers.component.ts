import { Component, OnInit } from '@angular/core';
import { ApiService } from  '../core/api.service';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";


@Component({
  selector: 'app-fizzbuzz-numbers',
  templateUrl: './fizzbuzz-numbers.component.html',
  styleUrls: ['./fizzbuzz-numbers.component.css']
  
})
export class FizzbuzzNumbersComponent implements OnInit {

  fizzbuzznumbers:  Array<object> = [];
  actualNumbers:string;

  ButtonTextChangedToReset:boolean = false;

  fizzbuzzForm: FormGroup;
 
   constructor(private formBuilder: FormBuilder, private apiService: ApiService) { }
  ngOnInit() {
    this.fizzbuzzForm = this.formBuilder.group({
      txtActualNumbers: ['', [Validators.required, Validators.pattern('^-?[0-9]+(,-?[0-9]+)*$')]]      
    });
  }

    public getfizzbuzzNumbers(){

      if(this.fizzbuzzForm.status == 'VALID')
     {
       if(this.ButtonTextChangedToReset==false)
       {
        this.actualNumbers = this.fizzbuzzForm.controls['txtActualNumbers'].value;
        this.apiService.getfizzbuzzNumbers(this.actualNumbers).subscribe((data:  Array<object>) => {
        this.fizzbuzznumbers  =  data;
        this.ButtonTextChangedToReset= true;
        });
      }else{
        this.ButtonTextChangedToReset= false;
        this.fizzbuzznumbers=[];
        this.fizzbuzzForm.controls['txtActualNumbers'].setValue(null);

      }
     }else{
      alert('Please enter in the textbox only a number or numbers separated by a comma \n(Negative numbers are also allowed)!')
     }


    }
}


