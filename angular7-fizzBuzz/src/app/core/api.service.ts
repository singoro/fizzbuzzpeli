import { Injectable } from '@angular/core';
import { HttpClient} from  '@angular/common/http';

@Injectable()
export class ApiService {
 
  baseUrl  =  'http://localhost:8080';
  constructor(private  httpClient:  HttpClient) { }
 

  getfizzbuzzNumbers(actualNumbers:string){
    return  this.httpClient.get(`${this.baseUrl}`+'/play/'+ actualNumbers );
  }
}